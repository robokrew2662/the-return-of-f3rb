# Robokrew Code 2022
Robokrew code repo for FRC game Rapid React 2022

## How to get Code onto the Robot from Gitlab Repo
1. Confirm your code is committed in the gitlab repo
2. Open git-bash tool found on the C drive of the team laptop
3. Change directory to the code directory on the team laptop
4. fetch latest meta data about repo (new branches for example)
	  1. git fetch
5. checkout to the branch you would like to deploy
	  1. git checkout \<branch-Name>
6. pull down the latest code
	  1. git pull
7. Open up VS code on team laptop
8. Connect Laptop to robot(ethernet,wirelessly,etc)
9. Run the WPI deploy
10. Drink Dew and pray it's gonna work

## How to get Code From team laptop to GitLab
1. Open git-bash tool found on the C drive of the team laptop
2. Change directory to the code directory on the team laptop
3. Confirm you on the right branch (see "check out steps" above)
4. Staged the Changed files
	  1. git add *
	  2. git commit *
7. push the code to gitlab
	  1. git push

## How to get Code From GitLab to new laptop(Assumes GIT is installed)
1. Open git-bash tool found on the C drive of the laptop
2. Clone the repo onto the laptop
  1. git clone https://gitlab.com/robokrew2662/the-return-of-f3rb.git

## Setup New Laptop
1. Install Git
  1. https://git-scm.com/
2. Setup Git
  1. run these commands 
  	1. git config --global user.name "Robo Krew" && git config --global user.email "robokrew2662@gmail.com"
2. Install WPILib, This will have the VSCODE as well as default libraries for the robot
  1. https://docs.wpilib.org/en/stable/docs/zero-to-robot/step-2/wpilib-setup.html
3. Install CRT libraries (used by our motors)
  1. https://docs.ctre-phoenix.com/en/latest/ch05a_CppJava.html#frc-c-java-add-phoenix 
4. Follow steps to get code onto new laptop


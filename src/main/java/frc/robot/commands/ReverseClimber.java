package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ClimberSubsystem;

public class ReverseClimber extends CommandBase {
    private final ClimberSubsystem m_climberSubsystem;

    public ReverseClimber(ClimberSubsystem subsystem) {
        m_climberSubsystem = subsystem;
        addRequirements(m_climberSubsystem);

    }
    @Override
    public void initialize() {
        m_climberSubsystem.reverseClimber();

    }
    @Override
    public boolean isFinished(){
        return true;
    }
}




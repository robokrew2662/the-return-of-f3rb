package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveSubsystem; 

public class ReverseDriveOff extends CommandBase {
    
    public DriveSubsystem m_drive;

    public ReverseDriveOff( DriveSubsystem drive){
      
        m_drive = drive;

    }

    @Override
    public void execute() {
       m_drive.reverseDriveOff();

    }
    @Override
    public boolean isFinished() {
        return true;

    }
}

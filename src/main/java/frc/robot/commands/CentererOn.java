package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.CentererSubsystem;

public class CentererOn extends CommandBase {
    private final CentererSubsystem m_centererSubsystem;

    public CentererOn(CentererSubsystem subsystem) {
        m_centererSubsystem = subsystem;
        addRequirements(m_centererSubsystem);

    }
    @Override
    public void initialize() {
        m_centererSubsystem.turnOnCenterer();

    }
    @Override
    public boolean isFinished(){
        return true;
    }
}




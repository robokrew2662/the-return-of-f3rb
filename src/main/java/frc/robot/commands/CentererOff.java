package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.CentererSubsystem;

public class CentererOff extends CommandBase {
    private final CentererSubsystem m_centererSubsystem;

    public CentererOff(CentererSubsystem subsystem) {
        m_centererSubsystem = subsystem;
        addRequirements(m_centererSubsystem);

    }
    @Override
    public void initialize() {
        m_centererSubsystem.turnOffCenterer();

    }
    @Override
    public boolean isFinished(){
        return true;
    }
}




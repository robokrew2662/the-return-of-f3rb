package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.Constants;
import frc.robot.subsystems.CentererSubsystem;
import frc.robot.subsystems.DriveSubsystem;  
import frc.robot.subsystems.IndexterSubsystem;
import frc.robot.subsystems.IntakeSubsystem;
import frc.robot.subsystems.ShooterSubsystem;

public class Autonomous extends SequentialCommandGroup{
    public Autonomous(DriveSubsystem drive, IntakeSubsystem intake, IndexterSubsystem index, ShooterSubsystem shooter, CentererSubsystem centerer) {
        addCommands(
            new ChangeShooterState(shooter),
            new AutoDrive(Constants.AUTO_DRIVE_SPEED, drive, Constants.AUTO_DRIVE_LOOP_MAX),
            new AutoDriveStop(drive),
            new IndexterOn(index)
        );
    }
}
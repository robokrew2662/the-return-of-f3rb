package frc.robot.commands;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveSubsystem;  

/**
 * A command to drive the robot with joystick input (passed in as {@link DoubleSupplier}s). Written
 * explicitly for pedagogical purposes - actual code should inline a command this simple with {@link
 * edu.wpi.first.wpilibj2.command.RunCommand}.
 */
public class ReverseDrive extends CommandBase {
  private final DriveSubsystem m_drive;
  private final double m_forward;
  private final double  m_rotation;

  /**
   * Creates a new DefaultDrive.
   *
   * @param d The drive subsystem this command wil run on.
   * @param e The control input for driving forwards/backwards
   * @param m_robotDrive The control input for turning
   */
  public ReverseDrive (XboxController driveJoy , DriveSubsystem m_robotDrive) {
    m_drive = m_robotDrive;
    m_forward = driveJoy.getLeftY();
    m_rotation = driveJoy.getRightX();
    addRequirements(m_drive);
  }

@Override
  public void execute() {
      m_drive.drive(-1*m_forward, -1*m_rotation);
  }
}
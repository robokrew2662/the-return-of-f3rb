package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.ShooterSubsystem;

public class ShooterSpeedIncrease extends CommandBase {
    private final ShooterSubsystem m_shooterSubsystem;

    public ShooterSpeedIncrease(ShooterSubsystem subsystem) {
        m_shooterSubsystem = subsystem;
        addRequirements(m_shooterSubsystem);

    }

    @Override
    public void execute() {
        m_shooterSubsystem.setSpeed(Constants.SHOOTER_INCREASE_SPEED);
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
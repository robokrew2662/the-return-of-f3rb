package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ClimberSubsystem;

public class ClimberOn extends CommandBase {
    private final ClimberSubsystem m_climberSubsystem;

    public ClimberOn(ClimberSubsystem subsystem) {
        m_climberSubsystem = subsystem;
        addRequirements(m_climberSubsystem);

    }
    @Override
    public void initialize() {
        m_climberSubsystem.turnOnClimber();

    }
    @Override
    public boolean isFinished(){
        return true;
    }
}




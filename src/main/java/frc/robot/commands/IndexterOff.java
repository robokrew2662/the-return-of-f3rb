package frc.robot.commands;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.IndexterSubsystem;

/**
 * A command to drive the robot with joystick input (passed in as {@link DoubleSupplier}s). Written
 * explicitly for pedagogical purposes - actual code should inline a command this simple with {@link
 * edu.wpi.first.wpilibj2.command.RunCommand}.
 */
public class IndexterOff extends CommandBase {
    private final IndexterSubsystem m_index;

    public IndexterOff(IndexterSubsystem indexter) {
    m_index = indexter;
    addRequirements(m_index);
    }

    @Override
    public void execute() {
        
        m_index.turnMotorOff();

    }

    @Override
    public boolean isFinished()
    {
        return true;
    }


}
package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.IntakeSubsystem;

public class ReleaseIntake extends CommandBase {
    private final IntakeSubsystem m_IntakeSubsystem;

    public ReleaseIntake(IntakeSubsystem subsystem) {
        m_IntakeSubsystem = subsystem;
        addRequirements(m_IntakeSubsystem);

    }
    @Override
    public void initialize() {
        m_IntakeSubsystem.releaseIntake();

    }
    @Override
    public boolean isFinished(){
        return true;
    }
}




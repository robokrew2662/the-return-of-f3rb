package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ClimberSubsystem;

public class ClimberOff extends CommandBase {
    private final ClimberSubsystem m_climberSubsystem;

    public ClimberOff(ClimberSubsystem subsystem) {
        m_climberSubsystem = subsystem;
        addRequirements(m_climberSubsystem);

    }
    @Override
    public void initialize() {
        m_climberSubsystem.turnOffClimber();

    }
    @Override
    public boolean isFinished(){
        return true;
    }
}




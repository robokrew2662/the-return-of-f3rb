package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.IntakeSubsystem;

public class PullInIntake extends CommandBase {
    private final IntakeSubsystem m_IntakeSubsystem;

    public PullInIntake(IntakeSubsystem subsystem) {
        m_IntakeSubsystem = subsystem;
        addRequirements(m_IntakeSubsystem);

    }
    @Override
    public void initialize() {
        m_IntakeSubsystem.pullInIntake();

    }
    @Override
    public boolean isFinished(){
        return true;
    }
}




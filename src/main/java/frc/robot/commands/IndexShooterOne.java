package frc.robot.commands;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.IndexterSubsystem;

/**
 * A command to drive the robot with joystick input (passed in as {@link DoubleSupplier}s). Written
 * explicitly for pedagogical purposes - actual code should inline a command this simple with {@link
 * edu.wpi.first.wpilibj2.command.RunCommand}.
 */
public class IndexShooterOne extends CommandBase {
    private final IndexterSubsystem m_index;

    public IndexShooterOne(IndexterSubsystem indexter) {
        m_index = indexter;
        addRequirements(m_index);
    }

    @Override
    public void initialize() {
        
        m_index.turnMotorOn();

    }

    @Override
    public boolean isFinished()
    {
        return m_index.oneBallOut();
    }

    @Override
    public void end(boolean interrupted) {
        m_index.turnMotorOff();
    }

}
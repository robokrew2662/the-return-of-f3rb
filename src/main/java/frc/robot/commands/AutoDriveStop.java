package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveSubsystem;  

public class AutoDriveStop extends CommandBase{
    private final DriveSubsystem m_drive;

    public AutoDriveStop(DriveSubsystem drive){
        m_drive = drive;

    }

    @Override
    public void execute() {
        m_drive.drive(0, 0);


    }
    @Override
    public boolean isFinished() {
        return true;

    }
}
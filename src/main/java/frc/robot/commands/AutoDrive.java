package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveSubsystem; 

public class AutoDrive extends CommandBase{
    private final DriveSubsystem m_drive;
    private final double m_speed;
    private final double m_AUTO_DRIVE_TIME_MAX;
    private double currentDriveTimer;

    public AutoDrive(double speed, DriveSubsystem drive, double driveTimerMax){
        m_speed = speed;
        m_drive = drive;
        m_AUTO_DRIVE_TIME_MAX = driveTimerMax;
        currentDriveTimer = 0;
    }

    @Override
    public void execute() {
        m_drive.directDrive(m_speed, 0);
        currentDriveTimer++;

    }
    @Override
    public boolean isFinished() {
        return (currentDriveTimer >= m_AUTO_DRIVE_TIME_MAX);

    }
}
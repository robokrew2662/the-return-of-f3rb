package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveSubsystem; 

public class ReverseDriveOn extends CommandBase {
    public DriveSubsystem m_drive;

    public ReverseDriveOn( DriveSubsystem drive){
      
        m_drive = drive;

    }

    @Override
    public void execute() {
       m_drive.reverseDriveOn();

    }
    @Override
    public boolean isFinished() {
        return true;

    }

}

package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.ShooterSubsystem;

public class ChangeShooterState extends CommandBase {
    private final ShooterSubsystem m_shooterSubsystem;

    public ChangeShooterState(ShooterSubsystem subsystem) {
        m_shooterSubsystem = subsystem;
        addRequirements(m_shooterSubsystem);

    }

    @Override
    public void execute() {
        if (m_shooterSubsystem.getSpeed() < 0)
        {
            m_shooterSubsystem.turnOffShooter();
        }
        else
        {
            m_shooterSubsystem.turnOnShooter(Constants.SHOOTER_MAX_SPEED);
        }
    }

    @Override 
    public boolean isFinished()
    {
        return true;
    }
}
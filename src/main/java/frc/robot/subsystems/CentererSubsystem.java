package frc.robot.subsystems;

import edu.wpi.first.wpilibj.motorcontrol.VictorSP;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class CentererSubsystem extends SubsystemBase {
    VictorSP centererMotor = new VictorSP(Constants.CENTERER_PWM_PORT);

    public void turnOnCenterer() {
        centererMotor.set(Constants.CENTER_MOTOR_SPEED);
    }

    public void turnOffCenterer() {
        centererMotor.set(0);
    }

    public void reverseCenterer() {
        centererMotor.set(-Constants.CENTER_MOTOR_SPEED);
    }
}